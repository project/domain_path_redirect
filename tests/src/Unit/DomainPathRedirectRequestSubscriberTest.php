<?php

declare(strict_types=1);

namespace Drupal\Tests\domain_path_redirect\Unit;

use Drupal\Core\Language\Language;
use Drupal\domain\DomainInterface;
use Drupal\domain_path_redirect\Entity\DomainPathRedirect;
use Drupal\domain_path_redirect\EventSubscriber\DomainPathRedirectRequestSubscriber;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;

/**
 * Tests the domain path redirect logic.
 *
 * @group redirect
 *
 * @coversDefaultClass \Drupal\domain_path_redirect\EventSubscriber\DomainPathRedirectRequestSubscriber
 */
class DomainPathRedirectRequestSubscriberTest extends UnitTestCase {

  /**
   * Data provider for both tests.
   *
   * @return array
   *   Nested arrays of values to check:
   *   - $request_uri
   *   - $request_query
   *   - $redirect_uri
   *   - $redirect_query
   *   - $hostname
   */
  public function getDomainPathRedirectData() {
    return [
      [
        'non-existing',
        ['key' => 'val'],
        '/test-path',
        ['dummy' => 'value'],
        'example1.com',
        'example1_com',
      ],
      [
        'non-existing/',
        ['key' => 'val'],
        '/test-path',
        ['dummy' => 'value'],
        'example2.com',
        'example1_com',
      ],
    ];
  }

  /**
   * @covers ::onKernelRequestCheckDomainPathRedirect
   * @dataProvider getDomainPathRedirectData
   */
  public function testRedirectLogicWithQueryRetaining($request_uri, $request_query, $redirect_uri, $redirect_query, $hostname, $domain_id) {

    // The expected final query. This query must contain values defined
    // by the redirect entity and values from the accessed url.
    $final_query = $redirect_query + $request_query;

    $url = $this->createMock('Drupal\Core\Url');

    $url->expects($this->once())
      ->method('setAbsolute')
      ->with(TRUE)
      ->willReturn($url);

    $url->expects($this->once())
      ->method('getOption')
      ->with('query')
      ->willReturn($redirect_query);

    $url->expects($this->once())
      ->method('setOption')
      ->with('query', $final_query);

    $url->expects($this->once())
      ->method('toString')
      ->willReturn($redirect_uri);

    $domain = $this->getDomainStub($hostname);
    $redirect = $this->getRedirectStub($url, $domain_id);

    $event = $this->callOnKernelRequestCheckDomainPathRedirect($redirect, $domain, $request_uri, $request_query, TRUE);

    $this->assertTrue($event->getResponse() instanceof RedirectResponse);
    $response = $event->getResponse();
    $this->assertEquals('/test-path', $response->getTargetUrl());
    $this->assertEquals(301, $response->getStatusCode());
    $this->assertEquals(1, $response->headers->get('X-Redirect-ID'));
  }

  /**
   * @covers ::onKernelRequestCheckDomainPathRedirect
   * @dataProvider getDomainPathRedirectData
   */
  public function testDomainPathRedirectLogicWithoutQueryRetaining($request_uri, $request_query, $redirect_uri, $redirect_query, $hostname, $domain_id) {
    $url = $this->createMock('Drupal\Core\Url');

    $url->expects($this->once())
      ->method('setAbsolute')
      ->with(TRUE)
      ->willReturn($url);

    // No query retaining, so getOption should not be called.
    $url->expects($this->never())
      ->method('getOption');
    $url->expects($this->never())
      ->method('setOption');

    $url->expects($this->once())
      ->method('toString')
      ->willReturn($redirect_uri);

    $domain = $this->getDomainStub($hostname);
    $redirect = $this->getRedirectStub($url, $domain_id);
    $event = $this->callOnKernelRequestCheckDomainPathRedirect($redirect, $domain, $request_uri, $request_query, FALSE);

    $this->assertTrue($event->getResponse() instanceof RedirectResponse);
    $response = $event->getResponse();
    $this->assertEquals($redirect_uri, $response->getTargetUrl());
    $this->assertEquals(301, $response->getStatusCode());
    $this->assertEquals(1, $response->headers->get('X-Redirect-ID'));
  }

  /**
   * Gets the domain mock object.
   *
   * @param string $hostname
   *   Url to be returned from getHostname.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   The mocked domain object.
   */
  protected function getDomainStub($hostname) {
    $domain = $this->createMock('Drupal\domain\Entity\Domain');

    $domain->expects($this->any())
      ->method('getHostname')
      ->with($hostname)
      ->willReturn($domain);

    return $domain;
  }

  /**
   * Gets the redirect mock object.
   *
   * @param string $url
   *   Url to be returned from getRedirectUrl.
   * @param string $domain_id
   *   Domain to be returned from getDomain.
   * @param int $status_code
   *   The redirect status code.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   The mocked redirect object.
   */
  protected function getRedirectStub($url, $domain_id, $status_code = 301) {
    $redirect = $this->createMock('Drupal\domain_path_redirect\Entity\DomainPathRedirect');

    $redirect->expects($this->once())
      ->method('getRedirectUrl')
      ->will($this->returnValue($url));
    $redirect->expects($this->any())
      ->method('getStatusCode')
      ->will($this->returnValue($status_code));
    $redirect->expects($this->any())
      ->method('id')
      ->willReturn(1);
    $redirect->expects($this->any())
      ->method('getDomain')
      ->willReturn($domain_id);
    $redirect->expects($this->once())
      ->method('getCacheTags')
      ->willReturn(['domain_path_redirect:1']);

    return $redirect;
  }

  /**
   * Instantiates the subscriber and runs onKernelRequestCheckRedirect()
   *
   * @param \Drupal\domain_path_redirect\Entity\DomainPathRedirect $redirect
   *   The redirect entity.
   * @param \Drupal\domain\DomainInterface $domain
   *   A domain record object.
   * @param string $request_uri
   *   The URI of the request.
   * @param array $request_query
   *   The query that is supposed to come via request.
   * @param bool $retain_query
   *   Flag if to retain the query through the redirect.
   *
   * @return \Symfony\Component\HttpKernel\Event\RequestEvent
   *   The response event.
   */
  protected function callOnKernelRequestCheckDomainPathRedirect(DomainPathRedirect $redirect, DomainInterface $domain, $request_uri, array $request_query, $retain_query) {
    $event = $this->getGetResponseEventStub($request_uri, http_build_query($request_query));
    $request = $event->getRequest();

    $checker = $this->createMock('Drupal\redirect\RedirectChecker');
    $checker->expects($this->any())
      ->method('canRedirect')
      ->will($this->returnValue(TRUE));

    $context = $this->createMock('Symfony\Component\Routing\RequestContext');

    $inbound_path_processor = $this->createMock('Drupal\Core\PathProcessor\InboundPathProcessorInterface');
    $inbound_path_processor->expects($this->any())
      ->method('processInbound')
      ->with($request->getPathInfo(), $request)
      ->will($this->returnValue($request->getPathInfo()));

    $alias_manager = $this->createMock('Drupal\path_alias\AliasManagerInterface');
    $module_handler = $this->createMock('Drupal\Core\Extension\ModuleHandlerInterface');
    $entity_manager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $logger = $this->createMock('Drupal\Core\Logger\LoggerChannelFactoryInterface');
    $url_generator = $this->createMock('Drupal\Core\Routing\UrlGeneratorInterface');
    $messenger = $this->createMock('Drupal\Core\Messenger\MessengerInterface');
    $route_match = $this->createMock('Drupal\Core\Routing\RouteMatchInterface');

    $subscriber = new DomainPathRedirectRequestSubscriber(
      $this->getDomainPathRedirectRepositoryStub('findMatchingRedirect', $redirect),
      $this->getLanguageManagerStub(),
      $this->getConfigFactoryStub(['redirect.settings' => ['passthrough_querystring' => $retain_query]]),
      $alias_manager,
      $module_handler,
      $entity_manager,
      $checker,
      $context,
      $inbound_path_processor,
      $this->getDomainNegotiatorStub('getActiveDomain', $domain),
      $logger,
      $url_generator,
      $messenger,
      $route_match
    );

    // Run the main redirect method.
    $subscriber->onKernelRequestCheckDomainPathRedirect($event);
    return $event;
  }

  /**
   * Gets the domain negotiator mock object.
   *
   * @param string $method
   *   Method to mock - either load() or findMatchingRedirect().
   * @param \Drupal\domain\DomainInterface $domain
   *   The domain object to be returned.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   The domain negotiator.
   */
  protected function getDomainNegotiatorStub($method, DomainInterface $domain) {
    $domain_negotiator = $this->createMock('Drupal\domain\DomainNegotiatorInterface');

    $domain_negotiator->expects($this->any())
      ->method($method)
      ->will($this->returnValue($domain));

    return $domain_negotiator;
  }

  /**
   * Gets the domain path redirect repository mock object.
   *
   * @param string $method
   *   Method to mock - either load() or findMatchingRedirect().
   * @param \Drupal\domain_path_redirect\Entity\DomainPathRedirect $redirect
   *   The redirect entity to be returned.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   The redirect repository.
   */
  protected function getDomainPathRedirectRepositoryStub($method, DomainPathRedirect $redirect) {
    $repository = $this->createMock('Drupal\domain_path_redirect\DomainPathRedirectRepository');

    $repository->expects($this->any())
      ->method($method)
      ->will($this->returnValue($redirect));

    return $repository;
  }

  /**
   * Gets post response event.
   *
   * @param array $headers
   *   Headers to be set into the response.
   *
   * @return \Symfony\Component\HttpKernel\Event\TerminateEvent
   *   The post response event object.
   */
  protected function getPostResponseEvent(array $headers = []) {
    $http_kernel = $this->createMock('Symfony\Component\HttpKernel\HttpKernelInterface');
    $request = $this->createMock('Symfony\Component\HttpFoundation\Request');

    $response = new Response('', 301, $headers);

    return new TerminateEvent($http_kernel, $request, $response);
  }

  /**
   * Gets response event object.
   *
   * @param string $path_info
   *   A string containing either an URI or a file or directory path.
   * @param string $query_string
   *   The query string of the request.
   *
   * @return \Symfony\Component\HttpKernel\Event\RequestEvent
   *   The response event.
   */
  protected function getGetResponseEventStub($path_info, $query_string) {
    $request = Request::create($path_info . '?' . $query_string, 'GET', [], [], [], ['SCRIPT_NAME' => 'index.php']);

    $http_kernel = $this->createMock('Symfony\Component\HttpKernel\HttpKernelInterface');
    return new RequestEvent($http_kernel, $request, NULL);
  }

  /**
   * Gets the language manager mock object.
   *
   * @return \Drupal\language\ConfigurableLanguageManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked language manager object.
   */
  protected function getLanguageManagerStub() {
    $language_manager = $this->createMock('Drupal\language\ConfigurableLanguageManagerInterface');
    $language_manager->expects($this->any())
      ->method('getCurrentLanguage')
      ->will($this->returnValue(new Language(['id' => 'en'])));

    return $language_manager;
  }

}
